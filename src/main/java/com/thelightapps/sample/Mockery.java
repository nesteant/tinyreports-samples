package com.thelightapps.sample;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Anton Nesterenko
 */
public class Mockery {


	public static List<String> getSimpleStringCollection() {
		List<String> l = new ArrayList<String>() {{
			add("a");
			add("b");
			add("c");
			add("d");
			add("e");
			add("f");
			add("g");
			add("h");
			add("i");
		}};

		Collections.shuffle(l);
		return l;
	}

	public static List<Car> getSampleCars(){
		List<Car> cars = new ArrayList<Car>(){{
			add(new Car("BMW", 100000000, 4, true));
			add(new Car("Mercedes", 25000000, 2, true));
			add(new Car("Porsche", 219879, 3, false));
			add(new Car("Audi", 10000, 4, true));
			add(new Car("Opel", 10000, 9, false));
			add(new Car("Ford", 10000, 5, true));
		}};
		Collections.shuffle(cars);
		return cars;
	}

	public static List<CarDetails> getSomeDetails(Car car){

		List<CarDetails> detailsList = new ArrayList<>();

		switch (car.getModel()){
			case "BMW": {
				detailsList.add(new CarDetails("bmwDetails", "another"));
				detailsList.add(new CarDetails("anotherDetails", "another1"));
				break;
			}
			case "Mercedes": {
				detailsList.add(new CarDetails("mersedesDetails", "another2"));
				detailsList.add(new CarDetails("mersedesDetails1", "another"));
				detailsList.add(new CarDetails("mersedesDetails2", "another"));
				break;
			}
			case "Porsche": {
				detailsList.add(new CarDetails("porsche1", "another"));
				detailsList.add(new CarDetails("porsche11", "another"));
				detailsList.add(new CarDetails("porsche111", "another"));
				break;
			}
			case "Audi": {
				break;
			}
			case "Opel": {
				break;
			}
			case "Ford": {
				break;
			}
		}
		return detailsList;
	}



	public static class Car {
		private String model;
		private long priceInCents;
		private int someOption;
		private boolean isNew;

		Car(String model, long priceInCents, int someOption, boolean aNew) {
			this.model = model;
			this.priceInCents = priceInCents;
			this.someOption = someOption;
			isNew = aNew;
		}

		public String getModel() {
			return model;
		}

		public void setModel(String model) {
			this.model = model;
		}

		public long getPriceInCents() {
			return priceInCents;
		}

		public void setPriceInCents(long priceInCents) {
			this.priceInCents = priceInCents;
		}

		public int getSomeOption() {
			return someOption;
		}

		public void setSomeOption(int someOption) {
			this.someOption = someOption;
		}

		public boolean isNew() {
			return isNew;
		}

		public void setNew(boolean aNew) {
			isNew = aNew;
		}
	}

	public static class CarDetails {
		private String someDetail;
		private String anotherDetail;

		public CarDetails(String someDetail, String anotherDetail) {
			this.someDetail = someDetail;
			this.anotherDetail = anotherDetail;
		}

		public String getSomeDetail() {
			return someDetail;
		}

		public void setSomeDetail(String someDetail) {
			this.someDetail = someDetail;
		}

		public String getAnotherDetail() {
			return anotherDetail;
		}

		public void setAnotherDetail(String anotherDetail) {
			this.anotherDetail = anotherDetail;
		}
	}
}
