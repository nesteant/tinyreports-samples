package com.thelightapps.sample;

import com.tinyreports.common.DataProvider;
import com.tinyreports.report.facade.TinyReportsGenerator;
import com.tinyreports.report.facade.TinyReportsRenderer;
import com.tinyreports.report.models.transfer.GroupingReport;

import java.io.Writer;

/**
 * @author Anton Nesterenko
 */
public class RelationReport extends AbstractSample {
	public static void main(String[] args) throws Exception {
		new RelationReport().exec();
	}

	@Override
	public void execute(Writer wr) throws Exception {
		DataProvider dataProvider = new DataProvider();
		dataProvider.putObject("mockery", Mockery.class);

		GroupingReport report = TinyReportsGenerator.generate(dataProvider, RelationReport.class.getResourceAsStream("/com/thelightapps/sample/relation.html"));
		TinyReportsRenderer.render(report, wr);
	}
}
