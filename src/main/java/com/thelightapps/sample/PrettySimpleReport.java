package com.thelightapps.sample;

import com.tinyreports.common.DataProvider;
import com.tinyreports.report.facade.TinyReportsGenerator;
import com.tinyreports.report.facade.TinyReportsRenderer;
import com.tinyreports.report.models.transfer.GroupingReport;

import java.io.Writer;

/**
 * @author Anton Nesterenko
 */
public class PrettySimpleReport extends AbstractSample {

	public static void main(String[] args) throws Exception {
		new PrettySimpleReport().exec();
	}

	@Override
	public void execute(Writer wr) throws Exception {
		DataProvider dataProvider = new DataProvider();
		dataProvider.putObject("REPORT_ITERATOR", Mockery.getSimpleStringCollection());

		GroupingReport report = TinyReportsGenerator.generate(dataProvider, PrettySimpleReport.class.getResourceAsStream("/com/thelightapps/sample/pretty-simple.html"));
		TinyReportsRenderer.render(report, wr);
	}
}
