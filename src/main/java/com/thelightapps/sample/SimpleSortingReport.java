package com.thelightapps.sample;

import com.tinyreports.common.DataProvider;
import com.tinyreports.report.facade.TinyReportsGenerator;
import com.tinyreports.report.facade.TinyReportsRenderer;
import com.tinyreports.report.models.transfer.GroupingReport;

import java.io.Writer;

/**
 * @author Anton Nesterenko
 */
public class SimpleSortingReport extends AbstractSample {

	public static void main(String[] args) throws Exception {
		new SimpleSortingReport().exec();
	}

	@Override
	public void execute(Writer wr) throws Exception {
		DataProvider dataProvider = new DataProvider();
		dataProvider.putObject("REPORT_ITERATOR", Mockery.getSimpleStringCollection());

		GroupingReport report = TinyReportsGenerator.generate(dataProvider, SimpleSortingReport.class.getResourceAsStream("/com/thelightapps/sample/simple-sorting.html"));
		TinyReportsRenderer.render(report, wr);
	}
}
