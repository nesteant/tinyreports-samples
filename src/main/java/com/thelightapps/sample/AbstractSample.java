package com.thelightapps.sample;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.Writer;

/**
 * @author Anton Nesterenko
 */
public abstract class AbstractSample {

	protected static final String DIR_NAME = "/tmp/";

	protected void exec() throws Exception {


		File dir = new File(DIR_NAME);
		if (dir.exists()) {
			try (FileWriter wr = new FileWriter(FileUtils.getFile(dir, "render.html"))) {
				execute(wr);
			}
		} else {
			throw new IllegalStateException("Please create directory you specified");
		}
	}

	public abstract void execute(Writer wr) throws Exception;
}
